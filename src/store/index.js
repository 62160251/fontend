import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogin: false
  },
  mutations: {
    login (state) {
      state.isLogin = true
    },
    logout (state) {
      state.isLogin = false
    }
  },
  actions: {
    login ({ commit }) {
      commit('login')
    },
    logout ({ commit }) {
      commit('logout')
    }
  },
  modules: {
    auth
  }
})
